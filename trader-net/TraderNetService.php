<?php

namespace App\Services\TraderNet;

use App\Services\TraderNet\Resources\BaseResource;
use App\Services\TraderNet\Resources\EndpointResources\HistoricalHLOCResource;
use App\Services\TraderNet\Resources\EndpointResources\PositionsResource;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;

/**
 * Class TraderNetService
 * @package App\Services
 *
 * @method getPosition(): PositionsResource | array
 * @method getStockQuotes(array $tickers): array
 * @method getHLOC(string $id, int $timeframe, string $date_from, string $date_to): Collection | array
 */
class TraderNetService
{
    public array $method = [];

    public const VERSION_FIRST = 1;
    public const VERSION_SECOND = 2;

    public const API_URL = 'https://tradernet.kz/api';

    public null|int $version = null;
    public string $publicKey;
    public bool $simplifiedOut = false;
    private string $secretKey;

    private null|string $SIDKey = null;

    public const TIMEFRAME_ONE_MINUTE = 1;
    public const TIMEFRAME_FIVE_MINUTES = 5;
    public const TIMEFRAME_FIFTEEN_MINUTES = 15;
    public const TIMEFRAME_ONE_HOUR = 60;
    public const TIMEFRAME_ONE_DAY = 1440;

    public const METHODS = [
        'getPosition' => [
            'version' => self::VERSION_SECOND,
            'fullMethodName' => 'getPositionJson',
            'resource' => PositionsResource::class
        ],
        'getStockQuotes' => [
            'version' => self::VERSION_SECOND,
            'fullMethodName' => 'getStockQuotesJson',
            'fields' => ['tickers'],
            'validationRules' => [
                'tickers' => ['required', 'array']
            ],
        ],
        'getHLOC' => [
            'version' => self::VERSION_SECOND,
            'fullMethodName' => 'getHloc',
            'fields' => ['id', 'timeframe', 'date_from', 'date_to'],
            'validationRules' => [
                'id' => ['required', 'string'],
                'count' => ['required', 'int'],
                'timeframe' => ['required', 'int', 'in:1,5,15,60,1440'],
                'intervalMode' => ['required', 'string'],
                'date_from' => ['required', 'string', 'regex:/[0-3][\d].[0-1][\d].[\d]{4}[ ][0-2][\d]:[0-5][\d]/'],
                'date_to' => ['required', 'string', 'regex:/[0-3][\d].[0-1][\d].[\d]{4}[ ][0-2][\d]:[0-5][\d]/'],
            ],
            'defaultValues' => [
                'count' => 0,
                'intervalMode' => 'ClosedRay'
            ],
            'resource' => HistoricalHLOCResource::class
        ]
    ];

    public function __construct()
    {
        $this->publicKey = config('app.trader_net.public_key');
        $this->secretKey = config('app.trader_net.secret_key');
    }

    /**
     * @throws ValidationException
     * @throws Exception
     */
    public function __call(string $name, array $arguments): BaseResource|array
    {
        if (!isset(self::METHODS[$name])) {
            throw new InvalidArgumentException('Method not found', 422);
        }

        $this->method = self::METHODS[$name];
        $this->setVersion();

        if (isset($this->method['fields'])) $arguments = array_combine($this->method['fields'], $arguments);
        if (isset($this->method['defaultValues'])) $arguments = $this->setDefaultValues($arguments);
        if (isset($this->method['validationRules'])) $this->validate($arguments);

        $payload = $this->sendRequest($arguments);

        if ($this->simplifiedOut && isset($this->method['resource'])) {
            $resource = new $this->method['resource']();
            $payload = $resource::getObject($payload['result'] ?? $payload);
        }
        return $payload;
    }

    private function setVersion()
    {
        $this->version = $this->method['version'];
    }

    public function setSimplifiedOut($simplifiedOut)
    {
        $this->simplifiedOut = $simplifiedOut;
    }


    private function setDefaultValues(array $arguments): array
    {
        foreach ($this->method['defaultValues'] as $key => $defaultValue) {
            $arguments[$key] = $arguments[$key] ?? $defaultValue;
        }
        return $arguments;
    }

    /**
     * @throws ValidationException
     */
    private function validate(array $arguments)
    {
        $validationRules = $this->method['validationRules'];
        $validator = Validator::make($arguments, $validationRules);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * @throws Exception
     */
    private function sendRequest(array $params = []): array
    {
        $request = [
            'cmd' => $this->method['fullMethodName']
        ];

        if ($params) {
            $request['params'] = $params;
        }

        if ($this->SIDKey) {
            $request['SID'] = $this->SIDKey;
        }

        if ($this->version != self::VERSION_FIRST && $this->publicKey) {
            $request['apiKey'] = $this->publicKey;
        }

        $request['nonce'] = microtime(true) * 10000;

        $curl = curl_init();
        $headers = [];

        if ($this->version == self::VERSION_FIRST) {
            $preSign = self::preSign($request);
            $request['sig'] = md5($preSign . $this->secretKey);
        } else {
            $headers[] = 'X-NtApi-Sig: ' . $this->calcSign($request);
        }

        if ($headers) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        $url = self::API_URL . ($this->version == self::VERSION_FIRST ? "" : "/v2/cmd/" . $this->method['fullMethodName']);

        $postFields = $this->version == self::VERSION_FIRST
            ? ['q' => json_encode($request)]
            : http_build_query($request);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $payload = curl_exec($curl);

        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($statusCode != 200) {
            throw new Exception('Something went wrong', $statusCode);
        }
        return json_decode($payload, true);
    }

    private function calcSign($query): bool|string
    {
        return hash_hmac('sha256', self::preSign($query), $this->secretKey);
    }

    private static function preSign(array $query): string
    {
        $payload = [];
        ksort($query);

        foreach ($query as $key => $value) {
            if (is_array($value)) {
                $value = self::preSign($value);
            }

            $payload[] = $key . '=' . $value;
        }
        return implode('&', $payload);
    }

}
