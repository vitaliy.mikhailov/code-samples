<?php

use App\Services\TraderNet\TraderNetService;
use \App\Services\TraderNet\Resources\EndpointResources\HistoricalHLOCResource;

$exchangeService = new TraderNetService();

// It's need for using resources. We can don't using it, but returning values will be array type
$exchangeService->setSimplifiedOut(true);

/** @var HistoricalHLOCResource $HLOC */
$HLOC = $exchangeService->getHLOC('FB.US', TraderNetService::TIMEFRAME_ONE_DAY, '15.08.2020 00:00', '16.08.2020 00:00');