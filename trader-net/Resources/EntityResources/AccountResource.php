<?php

namespace App\Services\TraderNet\Resources\EntityResources;

use App\Services\TraderNet\Resources\BaseResource;
use App\Services\TraderNet\Resources\ResourceInterface;
use JetBrains\PhpStorm\Pure;

/**
 * Class AccountResource
 * @package App\Services\TraderNet\Resources
 * @property string $currency Валюта счета
 * @property float $freeAmount Свободные средства
 * @property null|float $rateToBaseCurrency Курс валюты счета к базовой валюте
 */
class AccountResource extends BaseResource implements ResourceInterface
{

    public string $currency;
    public float $freeAmount;
    public null|float $rateToBaseCurrency;

    #[Pure] public static function getObject($model): self
    {
        $payload = new self();
        $payload->currency = $model['curr'];
        $payload->freeAmount = $model['s'];
        $payload->rateToBaseCurrency = $model['currval'] ?? null;
        return $payload;
    }

}
