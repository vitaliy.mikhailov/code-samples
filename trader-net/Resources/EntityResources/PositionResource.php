<?php

namespace App\Services\TraderNet\Resources\EntityResources;

use App\Services\TraderNet\Resources\BaseResource;
use App\Services\TraderNet\Resources\ResourceInterface;
use JetBrains\PhpStorm\Pure;

/**
 * Class PositionResource
 * @package App\Services\TraderNet\Resources
 * @property int $extId Уникальный идентификатор открытой позиции в системе TraderNet
 * @property string $ticker
 * @property string $name Название компании, выпустившей бумаги
 * @property string $currency Валюта откртытой позиции
 * @property float $unitCount Колличество бумаг в позиции
 *
 * @property float $openPriceForUnit Балансовая цена открытия позиции
 * @property float $openAmount Балансовая стоимость позиции
 *
 * @property float $currentPriceForUnit Балансовая цена позиции на текущий момент
 * @property float $currentAmount Балансовая стоимость позиции на текущий момент
 *
 * @property float $profit Текущая прибыль
 */
class PositionResource extends BaseResource implements ResourceInterface
{

    public int $extId;
    public string $ticker;
    public null|string $name;
    public string $currency;
    public float $unitCount;

    public float $openPriceForUnit;
    public float $openAmount;

    public float $currentPriceForUnit;
    public float $currentAmount;

    public float $profit;

    #[Pure] public static function getObject($model): self
    {
        $payload = new self();
        $payload->extId = $model['acc_pos_id'];
        $payload->ticker = $model['i'];
        $payload->name = $model['name'] . ' (' . $model['name2'] . ')';
        $payload->currency = $model['curr'];
        $payload->unitCount = $model['q'];

        $payload->openPriceForUnit = $model['bal_price_a'];
        $payload->openAmount = $model['open_bal'];

        $payload->currentPriceForUnit = $model['mkt_price'];
        $payload->currentAmount = $payload->unitCount * $payload->currentPriceForUnit;

        $payload->profit = $model['profit_close'];
        return $payload;
    }

}
