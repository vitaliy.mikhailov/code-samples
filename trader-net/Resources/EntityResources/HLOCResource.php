<?php

namespace App\Services\TraderNet\Resources\EntityResources;

use App\Services\TraderNet\Resources\BaseResource;
use App\Services\TraderNet\Resources\ResourceInterface;
use Illuminate\Support\Carbon;
use JetBrains\PhpStorm\Pure;

/**
 * Class PositionResource
 * @package App\Services\TraderNet\Resources
 * @property Carbon $dateTime Дата
 * @property float $high Максимум
 * @property float $low Минимум
 * @property float $open Открытие
 * @property float $close Закрытие
 * @property float $volume Объем
 */
class HLOCResource extends BaseResource implements ResourceInterface
{
    public Carbon $dateTime;
    public float $high;
    public float $low;
    public float $open;
    public float $close;
    public float $volume;

    #[Pure] public static function getObject($model): self
    {
        $payload = new self();
        $payload->dateTime = Carbon::createFromTimestampUTC($model['dt']);
        $payload->high = $model['hloc'][0];
        $payload->low = $model['hloc'][1];
        $payload->open = $model['hloc'][2];
        $payload->close = $model['hloc'][3];
        $payload->volume = $model['vl'];
        return $payload;
    }

}
