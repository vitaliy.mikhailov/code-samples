<?php

namespace App\Services\TraderNet\Resources\EntityResources;

use App\Services\TraderNet\Resources\BaseResource;
use App\Services\TraderNet\Resources\ResourceInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\Pure;

/**
 * Class PositionResource
 * @package App\Services\TraderNet\Resources
 * @property string $id Идентификатор
 * @property string $name Название
 * @property string $currency Валюта
 * @property float $minStep Минимальный шаг
 * @property Carbon $firstDate Стартовая дата. День листинга акции на бирже
 * @property Collection $HLOCCollection Набор свечей
 */
class HLOCTickerResource extends BaseResource implements ResourceInterface
{
    public string $id;
    public string $name;
    public string $currency;
    public float $minStep;
    public Carbon $firstDate;

    #[Pure] public static function getObject($model): self
    {
        $payload = new self();
        $payload->id = $model['id'];
        $payload->name = $model['short_name'];
        $payload->currency = $model['currency'];
        $payload->minStep = $model['min_step'];
        $payload->firstDate = Carbon::createFromFormat('d.m.Y', $model['firstDate'])->startOfDay();
        $payload->HLOCCollection = HLOCResource::getObjectList($model['hloc']);
        return $payload;
    }

}
