<?php

namespace App\Services\TraderNet\Resources\EndpointResources;

use App\Services\TraderNet\Resources\BaseResource;
use App\Services\TraderNet\Resources\EntityResources\AccountResource;
use App\Services\TraderNet\Resources\EntityResources\PositionResource;
use App\Services\TraderNet\Resources\ResourceInterface;
use Illuminate\Support\Collection;

/**
 * Class AccountResource
 * @package App\Services\TraderNet\Resources
 * @property Collection $accounts Счета
 * @property Collection $positions Открытые позиции
 */
class PositionsResource extends BaseResource implements ResourceInterface
{
    public Collection $accounts;
    public Collection $positions;

    public static function getObject($model): self
    {
        $payload = new self();
        $payload->accounts = AccountResource::getObjectList($model['ps']['acc']);
        $payload->positions = PositionResource::getObjectList($model['ps']['pos']);
        return $payload;
    }

}
