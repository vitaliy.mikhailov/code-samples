<?php

namespace App\Services\TraderNet\Resources\EndpointResources;

use App\Services\TraderNet\Resources\BaseResource;
use App\Services\TraderNet\Resources\EntityResources\HLOCTickerResource;
use App\Services\TraderNet\Resources\ResourceInterface;
use Illuminate\Support\Collection;

/**
 * Class AccountResource
 * @package App\Services\TraderNet\Resources
 * @property Collection $tickers Тикеры
 */
class HistoricalHLOCResource extends BaseResource implements ResourceInterface
{
    public Collection $tickers;

    public static function getObject($model): self
    {
        $payload = new self();
        $payload->tickers = collect();

        foreach ($model['info'] as $ticker => $tickerInfo) {
            $modelsList = [];
            foreach ($model['hloc'][$ticker] as $counter => $HLOCInfo) {
                $modelsList[] = [
                    'hloc' => $HLOCInfo,
                    'vl' => $model['vl'][$ticker][$counter],
                    'dt' => $model['xSeries'][$ticker][$counter],
                ];
            }
            $ticker = $model['info'][$ticker];
            $ticker['hloc'] = $modelsList;
            $ticker = HLOCTickerResource::getObject($ticker);
            $payload->tickers->put($ticker->id, $ticker);
        }

        return $payload;
    }

}
