<?php

namespace App\Services\TraderNet\Resources;

use Illuminate\Support\Collection;

interface ResourceInterface
{
    public static function getObject(array $model): self;

    public static function getObjectList(array $models): Collection;
}
