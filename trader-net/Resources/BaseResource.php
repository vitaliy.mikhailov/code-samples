<?php

namespace App\Services\TraderNet\Resources;

use Illuminate\Support\Collection;
use JetBrains\PhpStorm\Pure;

class BaseResource implements ResourceInterface
{

    #[Pure] public static function getObject(array $model): self
    {
        return new static();
    }

    public static function getObjectList(array $models): Collection
    {
        $payload = [];
        foreach ($models as $model) {
            array_push($payload, static::getObject($model));
        }
        return collect($payload);
    }
}
