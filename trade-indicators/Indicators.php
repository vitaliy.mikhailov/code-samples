<?php


namespace App\Helpers;


class Indicators
{

    public static function SMA(array $data, int $period): array
    {
        $prices = array_values($data);
        $dates = array_slice(array_keys($data), $period - 1);
        $total = count($prices);

        $new = $period;
        $drop = 0;
        $yesterday = 0;

        $payload[] = array_sum(array_slice($prices, 0, $period)) / $period;

        while ($new < $total) {
            $payload[] = round($payload[$yesterday] + ($prices[$new] / $period) - ($prices[$drop] / $period), 3);
            $drop++;
            $yesterday++;
            $new++;
        }

        return array_combine($dates, $payload);
    }

    public static function EMA(array $data, int $period): array
    {
        $prices = array_values($data);
        $dates = array_keys($data);
        $total = count($prices);
        $factor = 2 / ($period + 1);

        $payload = [$prices[0]];

        for ($i = 1; $i < $total; $i++) {
            $payload[] = round(($factor * $prices[$i]) + ((1 - $factor) * $payload[$i - 1]), 3);
        }

        return array_combine($dates, $payload);
    }

    public static function MACD(array $data, int $fastPeriod = 12, int $slowPeriod = 26, int $signalPeriod = 9): array
    {
        $fastEMA = self::EMA($data, $fastPeriod);
        $slowEMA = self::EMA($data, $slowPeriod);

        $MACD = [];

        foreach ($fastEMA as $date => $fastValue) {
            $MACD[$date] = round($fastValue - $slowEMA[$date], 3);
        }

        $signalEMA = self::EMA($MACD, $signalPeriod);

        $payload = [];

        foreach ($signalEMA as $date => $signalValue) {
            $payload['histogram'][$date] = round($MACD[$date] - $signalValue, 3);
        }

        $payload['sma'] = self::SMA($payload['histogram'], 10);

        return $payload;
    }

    public static function RSI(array $data, int $period = 14): array
    {
        $previous = $data[array_key_first($data)];
        $gains = [];
        $losses = [];
        $RSI = [];

        foreach ($data as $date => $value) {
            if ($previous - $value > 0) {
                $gains[$date] = 0;
                $losses[$date] = round($previous - $value, 3);
            } elseif ($previous - $value < 0) {
                $gains[$date] = round($value - $previous, 3);
                $losses[$date] = 0;
            } else {
                $gains[$date] = 0;
                $losses[$date] = 0;
            }

            $avgGain = count($gains) >= $period
                ? array_sum(array_slice($gains, count($gains) - $period, $period)) / $period
                : array_sum($gains) / count($gains);

            $avgLoss = count($losses) >= $period
                ? array_sum(array_slice($losses, count($losses) - $period, $period)) / $period
                : array_sum($losses) / count($losses);

            if ($avgLoss == 0 || $avgGain == 0) {
                $RSI[$date] = 50;
            } else {
                $RS = round($avgGain / $avgLoss, 3);
                $RSI[$date] = round(100 - 100 / (1 + $RS), 3);
            }

            $previous = $value;
        }

        return $RSI;
    }

}
