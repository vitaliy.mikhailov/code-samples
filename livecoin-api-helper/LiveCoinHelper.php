<?php


namespace App\Helpers;

/**
 * Публичые данные.
 *
 * @method mixed ticker(string $currencyPair = null) Получить информацию за последние 24 часа по конкретной паре валют.
 * @method mixed lastTrades(string $currencyPair, bool $minutesOrHour = false, string $type = false) Получить информацию о последних сделках (транзакциях) по заданной паре валют.
 * @method mixed orderBook(string $currencyPair, bool $groupByPrice = false, int $depth = null) Получить ордера по выбранной паре.
 * @method mixed allOrderBook(bool $groupByPrice = false, int $depth = 10) Возвращает ордербук по каждой валютной паре.
 * @method mixed getBidAsk(string $currencyPair = null) Возвращает максимальный бид и минимальный аск в текущем стакане.
 * @method mixed restrictions() Возвращает ограничения по каждой паре по мин. размеру ордера и максимальному кол-ву знаков после запятой в цене.
 * @method mixed coinInfo() Возвращает общую информацию по критовалютам.
 *
 *
 * Приватные данные пользователя.
 *
 * @method mixed trades(string $currencyPair = null, string $orderDesc = true, int $limit = 100, int $offset = 0) По конкретному клиенту получить информацию о его последних сделках.
 * @method mixed clientOrders(string $currencyPair = null, string $openClosed = 'ALL', int $issuedFrom = null, int $issuedTo = null, int $startRow = 0, int $endRow = 2147483646) По конкретному клиенту и по конкретной паре валют получить полную информацию о его ордерах.
 * @method mixed order(int $orderId) Получить информацию об ордере по его ID.
 * @method mixed balances(string $currency = null) Возвращает массив с балансами пользователя.
 * @method mixed balance(string $currency) Возвращает доступный баланс для выбранной валюты.
 * @method mixed transactions(string $start, string $end, string $types = null, int $limit = 100, int $offset = 0) Возвращает список транзакций пользователя.
 * @method mixed size(string $start, string $end, string $types = null) Возвращает количество транзакций пользователя с заданными параметрами.
 * @method mixed commission() Возвращает текущую комиссию пользователя.
 * @method mixed commissionCommonInfo() Возвращает текущую комиссию пользователя.
 *
 *
 * Создание и отмена ордеров.
 *
 * @method mixed buyLimit(string $currencyPair, float $price, float $quantity) Открыть ордер (лимитный) на покупку, определенной валюты.
 * @method mixed selLimit(string $currencyPair, float $price, float $quantity) Открыть ордер (лимитный) на продажу определенной валюты.
 * @method mixed buyMarket(string $currencyPair, float $quantity) Открыть ордер (рыночный) на покупку определенной валюты на заданное количество.
 * @method mixed sellMarket(string $currencyPair, float $quantity) Открыть ордер (рыночный) на покупку определенной валюты на заданное количество.
 * @method mixed cancelLimit(string $currencyPair, int $orderId) Отменить ордер.
 *
 *
 * Пополнение и вывод.
 *
 * @method mixed getAddress(string $currency) Получить адрес кошелька для пополнения баланса по выбранной криптовалюте.
 * @method mixed outCoin(float $amount, string $wallet) Отправляет запрос на вывод криптовалюты.
 * @method mixed outPayeer(float $amount, string $currency, int $wallet, string $protect = null, string $protect_code = null, string $protect_period = null) Отправляет запрос на вывод на счет Payeer.
 * @method mixed outCapitalist(float $amount, string $currency, string $wallet) Отправляет запрос на вывод на счет Capitalist.
 * @method mixed outAdvcash(float $amount, string $currency, string $wallet) Отправляет запрос на вывод на счет Advcash (с указанием e-mail или кошелька).
 * @method mixed outYandex(float $amount, string $currency, string $wallet) Отправляет запрос на вывод на счет Яндекс Деньги.
 * @method mixed outQiwi(float $amount, string $currency, string $account) Отправляет запрос на вывод Qiwi.
 * @method mixed outCard(float $amount, string $currency, string $account) Отправляет запрос на вывод на счет банковской карты Visa/Mastercard (только RUR).
 * @method mixed outMastercard(float $amount, string $currency, string $cardNumber, string $cardHolder, string $cardHolderCountry, string $cardHolderCity, string $cardHolderDOB, string $cardHolderMobilePhone) Отправляет запрос на вывод на банковскую карту Mastercard (только в USD/EUR).
 * @method mixed outPerfectmoney(float $amount, string $currency, string $account, string $protect_code = null, string $protect_period = null) Отправляет запрос на вывод на счет PerfectMoney.
 *
 *
 * Работа с ваучерами
 *
 * @method mixed voucherMake(float $amount, string $currency, string $description = null, string $forUser = null) Выпускает ваучер.
 * @method mixed voucherAmount(string $voucher_code) Возвращает сумму ваучера по его коду.
 * @method mixed voucherRedeem(string $voucher_code) Погашает ваучер.
 *
 */
class LiveCoinHelper
{

    public $url = 'https://api.livecoin.net';

    const HTTP_METHOD_GET = 'GET';
    const HTTP_METHOD_POST = 'POST';

    protected $token;
    protected $secret;
    protected $method;
    protected $data;

    public $methods = [

        /*Публичне данные*/

        'ticker' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/ticker', 'auth' => false, 'is_market' => false,
            'paramNames' => ['currencyPair']],
        'lastTrades' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/last_trades', 'auth' => false, 'is_market' => false,
            'paramNames' => ['currencyPair', 'minutesOrHour', 'type']],
        'orderBook' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/order_book', 'auth' => false, 'is_market' => false,
            'paramNames' => ['currencyPair', 'groupByPrice', 'depth']],
        'allOrderBook' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/all/order_book', 'auth' => false, 'is_market' => false,
            'paramNames' => ['groupByPrice', 'depth']],
        'getBidAsk' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/maxbid_minask', 'auth' => false, 'is_market' => false,
            'paramNames' => ['currencyPair']],
        'restrictions' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/restrictions', 'auth' => false, 'is_market' => false,
            'paramNames' => []],
        'coinInfo' => ['method' => self::HTTP_METHOD_GET, 'url' => '/info/coinInfo', 'auth' => false, 'is_market' => false,
            'paramNames' => []],


        /*Приватные данные пользователя*/

        'trades' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/trades', 'auth' => true, 'is_market' => true,
            'paramNames' => ['currencyPair', 'orderDesc', 'limit', 'offset']],
        'clientOrders' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/client_orders', 'auth' => true, 'is_market' => true,
            'paramNames' => ['currencyPair', 'orderDesc', 'limit', 'offset']],
        'order' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/order', 'auth' => true, 'is_market' => true,
            'paramNames' => ['orderId']],
        'balances' => ['method' => self::HTTP_METHOD_GET, 'url' => '/payment/balances', 'auth' => true, 'is_market' => false,
            'paramNames' => ['currency']],
        'balance' => ['method' => self::HTTP_METHOD_GET, 'url' => '/payment/balance', 'auth' => true, 'is_market' => false,
            'paramNames' => ['currency']],
        'transactions' => ['method' => self::HTTP_METHOD_GET, 'url' => '/payment/history/transactions', 'auth' => true, 'is_market' => false,
            'paramNames' => ['start', 'end', 'types', 'limit', 'offset']],
        'size' => ['method' => self::HTTP_METHOD_GET, 'url' => '/payment/history/size', 'auth' => true, 'is_market' => false,
            'paramNames' => ['start', 'end', 'types']],
        'commission' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/commission', 'auth' => true, 'is_market' => true,
            'paramNames' => []],
        'commissionCommonInfo' => ['method' => self::HTTP_METHOD_GET, 'url' => '/exchange/commissionCommonInfo', 'auth' => true, 'is_market' => true,
            'paramNames' => []],


        /*Создание и отмена ордеров*/

        'buyLimit' => ['method' => self::HTTP_METHOD_POST, 'url' => '/exchange/buylimit', 'auth' => true, 'is_market' => true,
            'paramNames' => ['currencyPair', 'price', 'quantity']],
        'sellLimit' => ['method' => self::HTTP_METHOD_POST, 'url' => '/exchange/selllimit', 'auth' => true, 'is_market' => true,
            'paramNames' => ['currencyPair', 'price', 'quantity']],
        'buyMarket' => ['method' => self::HTTP_METHOD_POST, 'url' => '/exchange/buyMarket', 'auth' => true, 'is_market' => true,
            'paramNames' => ['currencyPair', 'quantity']],
        'sellMarket' => ['method' => self::HTTP_METHOD_POST, 'url' => '/exchange/sellMarket', 'auth' => true, 'is_market' => true,
            'paramNames' => ['currencyPair', 'quantity']],
        'cancelLimit' => ['method' => self::HTTP_METHOD_POST, 'url' => '/exchange/cancellimit', 'auth' => true, 'is_market' => true,
            'paramNames' => ['currencyPair', 'orderId']],


        /*Пополнение и вывод*/

        'getAddress' => ['method' => self::HTTP_METHOD_GET, 'url' => '/payment/get/address', 'auth' => true, 'is_market' => false,
            'paramNames' => ['currency']],
        'outCoin' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/coin', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'wallet']],
        'outPayeer' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/payeer', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'wallet', 'protect', 'protect_code', 'protect_period']],
        'outCapitalist' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/capitalist', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'wallet']],
        'outAdvcash' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/advcash', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'wallet']],
        'outYandex' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/yandex', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'wallet']],
        'outQiwi' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/qiwi', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'account']],
        'outCard' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/card', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'account']],
        'outMastercard' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/mastercard', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'account']],
        'outPerfectmoney' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/out/perfectmoney', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'account', 'protect_code', 'protect_period']],


        /*Работа с ваучерами*/

        'voucherMake' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/voucher/make', 'auth' => true, 'is_market' => false,
            'paramNames' => ['amount', 'currency', 'description', 'forUser']],
        'voucherAmount' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/voucher/amount', 'auth' => true, 'is_market' => false,
            'paramNames' => ['voucher_code']],
        'voucherRedeem' => ['method' => self::HTTP_METHOD_POST, 'url' => '/payment/voucher/redeem', 'auth' => true, 'is_market' => false,
            'paramNames' => ['voucher_code']],

    ];

    public function __construct()
    {
        $this->token = config('app.livecoin_token');
        $this->secret = config('app.livecoin_secret');
    }

    public function request()
    {

        $options = [
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        // Получаем URL для запроса, собирая его из конфига и входных параметров
        $url = $this->url . $this->method['url'];

        // Построение параметров запроса
        $this->data = http_build_query($this->data, '', '&');


        // Авторизация
        if ($this->method['auth']) {

            // Сигнатура для заголовка (Этого требует Livecoin)
            $signature = strtoupper(hash_hmac('sha256', $this->data, $this->secret));

            $options[CURLOPT_HTTPHEADER] = [
                "Api-Key: " . $this->token,
                'Sign: ' . $signature
            ];
        }

        // Проверяем наличие входных параметров и метод передачи, если есть, обрабатываем их
        if (!empty($this->data) && $this->method['method'] == self::HTTP_METHOD_POST) {
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = $this->data;
        }

        // Обрабатываем наличие параметров для запроса. Если есть, то формируем URL и передаем поля, иначе просто голый URL
        $options[CURLOPT_URL] = ($this->method['method'] == self::HTTP_METHOD_GET && !empty($this->data)) ? $url . '?' . $this->data : $url;

        // Вернуть результат запроса cURL:
        $options[CURLOPT_RETURNTRANSFER] = true;

        // Инициальзируем cURL, устанавливаем параметры для него, собранные раннее и получаем ответ
        $curl = curl_init();
        curl_setopt_array($curl, $options);

        $returnTransfer = json_decode(curl_exec($curl), true);

        // Проверяем код ошибки
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // Если код не 200 (ОК) возвращаем код ошибки
        return $statusCode == 200 ? $returnTransfer : $statusCode;

    }

    public function __call(string $method, array $args = null)
    {

        if (!isset($this->methods[$method])) {
            return 400;
        }

        if (!is_null($args)) {
            $paramNames = array_slice($this->methods[$method]['paramNames'], 0, count($args));
            $args = array_combine($paramNames, $args);
            ksort($args);
            $this->data = $args;
        } else {
            $this->data = [];
        }

        $this->method = $this->methods[$method];

        return $this->request();

    }

}
