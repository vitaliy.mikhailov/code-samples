<?php

use App\Enums\StatusEnum;

// Use method all() from BaseEnum
$allStatuses = StatusEnum::all();

// Use one const from StatusEnum.
// We can change const value into StatusEnum class and const's value will change everywhere in the our project.
$oneStatus = StatusEnum::ENABLED;