<?php

// laravel\app\Enums
namespace App\Enums;

use ReflectionClass;

/**
 * Class BaseEnum
 * @package App\Enums
 */
class BaseEnum
{

    public static function all(): array
    {
        return (new ReflectionClass(static::class))->getConstants();
    }

}
